/**
 * Created by IntelliJ IDEA 2016.2
 * User: Rillox, Benedikt
 * vano. Agentur - www.vano-agentur.de
 * Date: 14.12.16
 * Time: 12:02
 * Copyright by vano. Agentur - Rillox, Benedikt
 **/

import java.util.Random;

public class LottoGesellschaft {
    private int[] gewinnZahlen = new int[Lottospiel.MAX_ANZ];

    private void sortieren() {
        for (int i = 1; i < gewinnZahlen.length; i++) {
            for (int j = gewinnZahlen.length - 1; j >= i; j--)
            if (gewinnZahlen[j] < gewinnZahlen[j - 1]) {
                int temp = gewinnZahlen[j];
                gewinnZahlen[j] = gewinnZahlen[j - 1];
                gewinnZahlen[j - 1] = temp;
            }
        }
    }

    public String toString() {

        String feld = "Gewinnzahlen: ";
        for (int i = 0; i < gewinnZahlen.length; i++) {
            feld = feld + gewinnZahlen[i] + " ";
        }
        return feld;
    }

    public void zieheGewinnzahlen() {
        Random r = new Random();
        int count = 0;
        while (count < gewinnZahlen.length) {
            gewinnZahlen[count] = 1 + Math.abs(r.nextInt()) % 49;
            for (int i = 0; i < count - 1; i++) {
                if (gewinnZahlen[i] == gewinnZahlen[count]) {
                    count --;
                }
            }
            count++;
        }
        sortieren();
    }

    public int pruefeTipp(int[] tipp) {
        int treffer = 0;
        for (int i = 0; i < tipp.length; i++) {
            for (int j = 0; j < gewinnZahlen.length; j++) {
                if (tipp[i] == gewinnZahlen[j]) {
                    treffer++;
                }
            }
        }
        return treffer;
    }
}

