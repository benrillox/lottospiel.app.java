import java.util.Scanner;

public class Lottospiel {

    public static final int MAX_ANZ = 6;

    public static void main(String[] arg) {

        Scanner eingabe = new Scanner(System.in);
        LottoGesellschaft westLotto = new LottoGesellschaft();

        LottoTipp leasTipp = new LottoTipp();
        System.out.println("Name der Lottospielerin: ");
        String name = eingabe.next();
        leasTipp.setName(name);

        int[] lottoZahlen = new int[MAX_ANZ];
        System.out.println("Bitte Tipp abgeben:");
        for (int i = 0; i < lottoZahlen.length; i++) {
            System.out.println("Bitte " + (i + 1) + " Zahl eingeben: ");
            lottoZahlen[i] = eingabe.nextInt();
        }

        leasTipp.setTipp(lottoZahlen); // Setter
        westLotto.zieheGewinnzahlen();
        System.out.println(westLotto.toString());
        System.out.println(leasTipp.toString());

        System.out.println("Anzahl Richtige: " + westLotto.pruefeTipp(leasTipp.getTipp())); // Getter

        LottoTipp[] spieler = new LottoTipp[5];
        for (int i = 0; i < spieler.length; i++) {
            spieler[i] = new LottoTipp();
            System.out.println("\nName der LottospielerIn: ");
            name = eingabe.next();
            spieler[i].setName(name);
            System.out.println("Bitte Tipp abgeben");
            for (int j = 0; j < lottoZahlen.length; j++) {
                System.out.println("Bitte " + (j + 1) + " . Zahl eingeben: ");
                lottoZahlen[j] = eingabe.nextInt();
            }
            spieler[i].setTipp(lottoZahlen); // Setter
        }
        westLotto.zieheGewinnzahlen(); // Ausziehung der Gewinnzahlen
        System.out.println(westLotto.toString());
        for (int i = 0; i < spieler.length; i++) {
            System.out.println(spieler[i].toString());
            System.out.println("Anzahl Richtige: " + westLotto.pruefeTipp(spieler[i].getTipp())); // Getter
        }
    }

}

