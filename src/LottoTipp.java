/**
 * Created by IntelliJ IDEA 2016.2
 * User: Rillox, Benedikt
 * vano. Agentur - www.vano-agentur.de
 * Date: 14.12.16
 * Time: 11:53
 * Copyright by vano. Agentur - Rillox, Benedikt
 **/


public class LottoTipp {

    private int[] tipp = new int[Lottospiel.MAX_ANZ];
    private String name;

    public void setTipp(int[] numbers) {

        for(int i = 0; i < numbers.length; i++) {
            tipp[i]=numbers[i];
        }
    }

    public int[] getTipp() {
        return tipp;
    }

    public void setName(String spieler) {
        name = spieler;
    }
    public String getName() {
        return name;
    }

    public String toString() {
        String feld = "Name: ";
        feld = feld + getName() + "\nTipp:";
        for (int i = 0; i < tipp.length; i++) {
            feld = feld + tipp[i] + " ";
        }
        return feld;
    }

}
